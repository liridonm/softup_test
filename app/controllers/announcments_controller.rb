class AnnouncmentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_annocuments, only: [:edit, :update, :destroy, :show]
  layout 'application'
  def index
    @annoucnments = Announcment.all

    # render json: @annoucnments

  end

  def new
    @annoucnment = current_user.announcments.new
  end

  def create
    @annoucnment = current_user.announcments.build(annoucment_params)

    if @annoucnment.save
      redirect_to root_path, notice: 'Annoucment has been created'
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @annoucnment.update_attributes(annoucment_params)
      redirect_to root_path, notice: 'Anncoucment has been updated'
    else
      render :edit
    end
  end

  def show
    @comments = @annoucnment.comments.all
    expires_in 3.minutes, :public => true
    # render json: @annoucnment
  end

  def destroy

  end

  private

  def annoucment_params
    params.require(:announcment).permit(:title, :body, :logo, :user_id)
  end

  def set_annocuments
    @annoucnment = Announcment.find(params[:id])
  end
end
