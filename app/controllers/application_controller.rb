class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?



  def after_sign_in_path_for(resource)
    if user_signed_in?
      root_path
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :department_id])

    devise_parameter_sanitizer.permit(:account_update) do |user_params|
      user_params.permit(:name, :department_id, :current_password, :role, :password, :password_confirmation)
    end
  end
end
