class CommentsController < ApplicationController
  before_action :authenticate_user!

  def new
    @annoucnment = Announcment.find(params[:id])
    @comment = @annoucnment.comments.build
  end

  def create
    @annoucnment = Announcment.find(params[:id])
    @comment = @annoucnment.comments.build(comment_params)

    if @comment.save
      puts 'liridon morina-----------------'
      redirect_to announcment_path(@annoucnment.id)
    else
      render :new
    end
  end

  private

  def comment_params
    params.permit(:content, :user_id, :announcment_id)
  end
end
