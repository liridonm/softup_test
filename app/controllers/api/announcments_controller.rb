class Api::AnnouncmentsController < Api::ApiController

    def create
      annoucnment = current_user.announcments.build(annoucment_params)

      if annoucnment.save
        render status: 200, json: {
            message: 'Successfully created an annoucment',
            annoucnment: annoucnment
        }.to_json
      else

        render status: 422, json: {
            errors: annoucnment.errors
        }.to_json

      end
    end


  def show
    announcment = Announcment.find(params[:id])

    if stale?(announcment)
      render json: announcment.as_json(only: [:id, :title, :body, :logo], include:{user: {only:[:id, :name], include: {department:{only: [:id,:name]}}},
                                                                                   comments: {only: [:content], include:{user:{only: [:id, :name], include:{department:{only: [:id, :name]}}}}}})
    end


  end

  private
  def annoucment_params
    params.require(:announcment).permit(:title, :body, :logo, :user_id)
  end
end
