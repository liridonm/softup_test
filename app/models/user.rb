class User < ApplicationRecord
  belongs_to :department
  has_many :comments, dependent: :destroy
  has_many :announcments, dependent: :destroy
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable
end
