class Announcment < ApplicationRecord
  belongs_to :user
  has_many :comments
  mount_uploader :logo, AvatarUploader
end
