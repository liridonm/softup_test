class AddUserToAnnouncment < ActiveRecord::Migration[5.1]
  def change
    add_reference :announcments, :user, index: true
  end
end
