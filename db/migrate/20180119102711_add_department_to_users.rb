class AddDepartmentToUsers < ActiveRecord::Migration[5.1]
  def change
    add_reference :users, :department, index: true
  end
end
