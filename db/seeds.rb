# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#

if Department.count.zero?
Department.create([{name: 'Information Technology'}, {name: 'Accounting'},
                   {name: 'Bank and finance'}, {name: 'Marketing'},
                   {name: 'Medical'}])

end
if User.count.zero?
u1 = User.new(id:1, email: 'admin1@softuptest.com', password: '123456', password_confirmation: '123456', name: 'Liridon Morina', department_id: Department.first.id)
u1.save
u1.confirm

u2 = User.new(id: 2,email: 'admin2@softuptest.com', password: '123456', password_confirmation: '123456', name: Faker::Name.name, department_id: Department.second.id)
u2.save
u2.confirm

u3 = User.new(id:3,email: 'admin3@softuptest.com', password: '123456', password_confirmation: '123456', name: Faker::Name.name, department_id: Department.third.id)
u3.save
u3.confirm

u4 = User.new(id:4,email: 'admin4@softuptest.com', password: '123456', password_confirmation: '123456', name: Faker::Name.name, department_id: Department.fourth.id)
u4.save
u4.confirm

u5 = User.new(id:5, email: 'admin5@softuptest.com', password: '123456', password_confirmation: '123456', name: Faker::Name.name, department_id: Department.fifth.id)
u5.save
u5.confirm
end

10.times do
Announcment.create!([
                         {title: Faker::Book.title, body: Faker::Lorem.sentence, logo: File.new('public/download.jpg'), user_id: User.find(1).id},
                         {title: Faker::Book.title,body: Faker::Lorem.sentence,logo: File.new('public/download.jpg'),user_id: User.find(2).id},
                         {title: Faker::Book.title,body: Faker::Lorem.sentence,logo: File.new('public/download.jpg'),user_id: User.find(3).id},
                         {title: Faker::Book.title,body: Faker::Lorem.sentence,logo: File.new('public/download.jpg'),user_id: User.find(4).id},
                         {title: Faker::Book.title,body: Faker::Lorem.sentence,logo: File.new('public/download.jpg'),user_id: User.find(5).id}])
end