require 'test_helper'

class AnnouncmentsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get announcments_index_url
    assert_response :success
  end

  test "should get new" do
    get announcments_new_url
    assert_response :success
  end

  test "should get edit" do
    get announcments_edit_url
    assert_response :success
  end

end
