Rails.application.routes.draw do

  namespace :api do
    resources :announcments
  end

  root 'announcments#index'
  post '/announcments/:id', to: 'comments#create', as: :new_comment

  resources :announcments
  devise_for :users, controllers: { sessions: 'users/sessions', passwords: 'users/passwords', confirmations: 'users/confirmations',
                                    registrations: 'users/registrations'}

end
