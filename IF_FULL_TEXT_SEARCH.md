#IF FULL TEXT SEARCH

Without any external library i can use paramaters for searching.
Parameters will show in url.


If I am not going to use Services like Agnolia or Elastic Search, for instance, I would perform searching depending on what I need and what I have. For instance, if the text would be small, I would search and find programatically. That would not be resources hungry and would work fast. Otherwise, We could follow the same strategy, for long texts but that would require more time and resources. In terms of what algorithms to use, I would go with Divide and Conquer, and binary search, as it ensures faster results if all text would be added to an array list.
